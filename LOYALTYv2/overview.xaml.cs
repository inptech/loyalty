﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Data.SqlClient;


namespace LOYALTYv2
{
    /// <summary>
    /// Interaction logic for overview.xaml
    /// </summary>
    public partial class overview : UserControl
    {
      

        SqlConnection connection = new SqlConnection(@"Data Source=DESKTOP-SVHP2N4\SQLEXPRESS; Initial Catalog=Loyalty; Integrated Security=True;");
        SqlCommand command;

        
        public overview()
        {
            InitializeComponent();
            LoadGrid();

        }

        public void LoadGrid()
        {
            SqlCommand cmd = new SqlCommand("select * from [tbl_users3] WHERE NOT Status='deleted'", connection);
            
            DataTable dt = new DataTable();
            connection.Open();
            SqlDataReader sdr = cmd.ExecuteReader();
            dt.Load(sdr);
            connection.Close();
            datagrid.ItemsSource = dt.DefaultView;
        }



        public void openConnection()
        {
            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }
        }
        public void closeConnection()
        {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
        }
        public void executeQuery(String query)
        {
            try
            {
                openConnection();
                command = new SqlCommand(query, connection);
                if (command.ExecuteNonQuery() == 1)
                {
                    MessageBox.Show("Query Executed");
                }
                else
                {
                    MessageBox.Show("Query Not Executed");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                closeConnection();
            }
        }
  

        private void delete_Click(object sender, RoutedEventArgs e)
        {
          


            string deleteQuery = "UPDATE [tbl_users3] SET Status = 'deleted' WHERE (Id = '" + textbox4.Text + "' AND Status='exists')";
            executeQuery(deleteQuery);
            closeConnection();

        }

        private void datagrid_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            DataGrid dg = sender as DataGrid;
            DataRowView dataRow = (DataRowView)dg.SelectedItem;
            int index = dg.CurrentCell.Column.DisplayIndex;
            string cellValue = dataRow.Row.ItemArray[0].ToString();

          
        }
    }
}
