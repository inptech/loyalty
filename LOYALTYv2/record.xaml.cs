﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Data.SqlClient;


namespace LOYALTYv2
{
    /// <summary>
    /// Interaction logic for record.xaml
    /// </summary>
    public partial class record : UserControl
    {

        
        SqlConnection connection = new SqlConnection(@"Data Source=DESKTOP-SVHP2N4\SQLEXPRESS; Initial Catalog=Loyalty; Integrated Security=True;");
        SqlCommand command;
      
        public record()
        {
            InitializeComponent();

           
        }

        public void openConnection()
        {
            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }
        }
        public void closeConnection()
        {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
        }
        public void executeQuery(String query)
        {
            try
            {
                openConnection();
                command = new SqlCommand(query, connection);
                if (command.ExecuteNonQuery() == 1)
                {
                    MessageBox.Show("Query Executed");
                }
                else
                {
                    MessageBox.Show("Query Not Executed");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                closeConnection();
            }
        }

        public void clearData()
        {
            txtbox1.Clear();
            txtbox2.Clear();
            txtbox3.Clear();
            txtbox4.Clear();
           
        }
        private void clearbtn2_Click(object sender, RoutedEventArgs e)
        {
            clearData();
        }

        private void txtbox3_TextChanged(object sender, TextChangedEventArgs e)
        {
            SqlConnection connection = new SqlConnection(@"Data Source=DESKTOP-SVHP2N4\SQLEXPRESS; Initial Catalog=Loyalty; Integrated Security=True");
            connection.Open();
            if (txtbox3.Text != "")
            {
                SqlCommand cmd = new SqlCommand("Select Ime, Prezime,Telefon from [tbl_users3] where Id =@Id", connection);
                cmd.Parameters.AddWithValue("@Id", int.Parse(txtbox3.Text));
                SqlDataReader da = cmd.ExecuteReader();
                while (da.Read())
                    
                {
                    txtbox1.Text = da.GetValue(0).ToString();
                    txtbox4.Text = da.GetValue(1).ToString();
                    txtbox2.Text = da.GetValue(2).ToString();
                }
                connection.Close();
            }

        }

        private static int i;

        private void button1_Click(object sender, RoutedEventArgs e)
        {

            SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-SVHP2N4\SQLEXPRESS; Initial Catalog=Loyalty; Integrated Security=True;");
            con.Open();
            SqlCommand cmd = new SqlCommand("Update tbl_users3 set Popust=@Popust where Id = @Id", con);
            cmd.Parameters.AddWithValue("@Id", int.Parse(txtbox3.Text));
            cmd.Parameters.AddWithValue("@Popust", int.Parse(txt10.Text));
            cmd.ExecuteNonQuery();
            MessageBox.Show("KUPOVINA USPJESNA");

            
           
           

            i++;
            txt10.Text = i.ToString();

            con.Close();


        }
    }
}
