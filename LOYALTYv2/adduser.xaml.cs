﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Data.SqlClient;


namespace LOYALTYv2
{
    /// <summary>
    /// Interaction logic for adduser.xaml
    /// </summary>
    public partial class adduser : UserControl
    {

        SqlConnection connection = new SqlConnection(@"Data Source=DESKTOP-SVHP2N4\SQLEXPRESS; Initial Catalog=Loyalty; Integrated Security=True;");
        SqlCommand command;


        public adduser()
        {
            InitializeComponent();
          
        }

        public void openConnection()
        {
            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }
        }
        public void closeConnection()
        {
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
        }
        public void executeQuery(String query)
        {
            try
            {
                openConnection();
                command = new SqlCommand(query, connection);
                if (command.ExecuteNonQuery() == 1)
                {
                    MessageBox.Show("Query Executed");
                }
                else
                {
                    MessageBox.Show("Query Not Executed");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                closeConnection();
            }
        }
        private void button1_Click(object sender, RoutedEventArgs e)
        {

            



            if (txtbox1.Text == "")
                MessageBox.Show("ENTER NAME");
            if (txtbox4.Text == "")
                MessageBox.Show("ENTER LASTNAME");
            if (txtbox2.Text == "")
                MessageBox.Show("ENTER ADRESS");
          
            if (txtbox5.Text == "")
                MessageBox.Show("ENTER PHONE");
            else { 

            string saveQuery = "INSERT INTO [tbl_users3] (Ime,Adresa,Prezime,Telefon,Status) VALUES ('" + txtbox1.Text + "', '" + txtbox2.Text + "','" + txtbox4.Text + "','" + txtbox5.Text + "','exists')";
            executeQuery(saveQuery);
            closeConnection();
        }

        }
        public void clearData()
        {
            txtbox1.Clear();
            txtbox2.Clear();
            
            txtbox4.Clear();
            txtbox5.Clear();
        }
        private void clearbtn_Click(object sender, RoutedEventArgs e)
        {
            clearData();
        }
    }
}
