﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Loyalty
{
    /// <summary>
    /// Interaction logic for record.xaml
    /// </summary>
    public partial class record : Window
    {
        public record()
        {
            InitializeComponent();
        }

        private void btn4_Click(object sender, RoutedEventArgs e)
        {
            Hide();
            new MainWindow().ShowDialog();
            ShowDialog();
        }
    }
}
